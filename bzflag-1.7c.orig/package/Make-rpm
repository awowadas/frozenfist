# bzflag
# Copyright 1993-1999, Chris Schoeneman
#
# This package is free software;  you can redistribute it and/or
# modify it under the terms of the license found in the file
# named LICENSE that should have accompanied this file.
#
# THIS PACKAGE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
# WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

DEPTH = ..
COMMONPREF = rpm
include $(DEPTH)/Make-common

RPM_NAME    = bzflag
RPM_VERSION = 1.7c
RPM_RELEASE = 2

RPM      = rpm
RPMDIR   = $(RPM)/tmp
RPMRC    = $(RPM)/rpmrc
SPECFILE = spec
RPMROOT  = $(RPMDIR)/root
TMPROOT  = /var/tmp/bzflag

LDIRT    = $(RPMRC)

#
# build RPM package
#
targets: rpmprep instprep
	strip $(TARGETDIR)/bzflag
	strip $(TARGETDIR)/bzfs
	rpm -ba --rcfile $(RPMRC) $(RPM)/$(SPECFILE)
	mv $(RPMDIR)/RPMS/$(ARCH)/*.rpm $(PACKAGEDIR)

#
# prepare RPM stuff
#
rpmprep:
	-$(RMR) $(RPMDIR) $(TMPROOT)
	$(MKDIR) $(RPMDIR) $(RPMDIR)/BUILD \
		$(RPMDIR)/RPMS $(RPMDIR)/RPMS/$(ARCH) \
		$(RPMDIR)/SOURCES $(RPMDIR)/SPECS $(RPMDIR)/SRPMS
	ln $(RPM)/$(SPECFILE) $(RPMDIR)/SPECS/$(SPECFILE)
	$(ECHO) "topdir: $(RPMDIR)" > $(RPMRC)
	ln -s `pwd`/$(RPMROOT) $(TMPROOT)

#
# populate fake install area
#
instprep:
	-$(RMR) $(RPMROOT)
	$(MKDIR) $(RPMROOT)
	$(MKDIR) $(RPMROOT)/usr
	$(MKDIR) $(RPMROOT)/usr/doc
	$(MKDIR) $(RPMROOT)/usr/local
	$(MKDIR) $(RPMROOT)/usr/local/bzflag
	$(MKDIR) $(RPMROOT)/usr/local/bzflag/bin
	$(MKDIR) $(RPMROOT)/usr/local/bzflag/data
	$(MKDIR) $(RPMROOT)/usr/local/man
	$(MKDIR) $(RPMROOT)/usr/local/man/man6
	$(MKDIR) $(RPMROOT)/usr/X11R6
	$(MKDIR) $(RPMROOT)/usr/X11R6/bin
	ln $(RPM)/README.linux $(RPMDIR)/BUILD/README
	ln $(DEPTH)/LICENSE $(RPMDIR)/BUILD/LICENSE
	ln $(DEPTH)/man/bzflag.6 $(RPMROOT)/usr/local/man/man6
	ln $(DEPTH)/man/bzfs.6 $(RPMROOT)/usr/local/man/man6
	ln $(DEPTH)/data/*.rgb $(RPMROOT)/usr/local/bzflag/data
	ln $(DEPTH)/data/*.wav $(RPMROOT)/usr/local/bzflag/data
	ln $(TARGETDIR)/bzfs $(RPMROOT)/usr/local/bzflag/bin/bzfs
	ln $(TARGETDIR)/bzflag $(RPMROOT)/usr/local/bzflag/bin/bzflag.real
	ln linux/bzflag.wrap.linux $(RPMROOT)/usr/local/bzflag/bin/bzflag
	ln -s /usr/local/bzflag/bin/bzflag $(RPMROOT)/usr/X11R6/bin/bzflag

clean: $(COMMONPREF)clean

clobber: $(COMMONPREF)clobber
	$(RMR) $(RPMDIR) $(TMPROOT)

